# base image
FROM python:3.8

# work dir
WORKDIR /app

# add azure ca certificate
ADD https://cacerts.digicert.com/DigiCertGlobalRootCA.crt.pem /app

# copy requir file
COPY requirements.txt .

# install requirements
RUN pip install -r requirements.txt

# copy code for work dir
COPY api.py .

# command to run on container start
CMD [ "flask", "run", "--host=0.0.0.0" ]
