import os
import sys
from flask import Flask, render_template, redirect, url_for, request, session, jsonify, send_from_directory, send_file
from werkzeug.utils import secure_filename
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy import text
from filehash import FileHash
from datetime import datetime
import json

UPLOAD_FOLDER = 'upload'
ALLOWED_EXTENSIONS = {'fasta'}

app = Flask(__name__)
CORS(app)

# upload
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.secret_key = os.urandom(24)

# database
MYSQL_HOST = os.environ.get("MYSQL_HOST")
MYSQL_USER = os.environ.get("MYSQL_USER")
MYSQL_PASSWORD = os.environ.get("MYSQL_PASSWORD")
MYSQL_DB = os.environ.get("MYSQL_DB")
MYSQL_SSL_CA_PATH = os.environ.get("MYSQL_SSL_CA_PATH")

# database
if "MYSQL_SSL_CA_PATH" in os.environ:
    app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://' + MYSQL_USER + ':' + MYSQL_PASSWORD + '@' + MYSQL_HOST + '/' + MYSQL_DB + '?ssl_ca=' + MYSQL_SSL_CA_PATH
else:
    app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://' + MYSQL_USER + ':' + MYSQL_PASSWORD + '@' + MYSQL_HOST + '/' + MYSQL_DB
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/', methods=['GET'])
def alive():
    return "alive"

@app.route('/fasta/<filename>', methods=['GET'])
def get_file(filename):
    return send_from_directory(UPLOAD_FOLDER, filename, as_attachment=True)

@app.route('/fasta/list', methods=['GET'])
def list_files():
    try:
        conn = db.engine.connect()
        records = conn.execute(text('SELECT filename,sequences,sha1,date_created from uploads order by filename;'))
        row_headers = ['filename', 'sequences', 'sha1', 'date_created']
        json_data=[]
        for record in records:
            json_data.append(dict(zip(row_headers,record)))
        return jsonify(files = json_data)
    except SQLAlchemyError as e:
        print(str(e.__dict__['orig']))
        return {'error': 'Error while listing'}
    finally:
        conn.close()

@app.route('/fasta/upload', methods=['POST'])
def upload_file():
    if request.method == 'POST':
        target = os.path.join(app.config['UPLOAD_FOLDER'])
        if not os.path.isdir(target):
            os.mkdir(target)
        file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            destination = "/".join([target, filename])
            session['uploadFilePath'] = destination
            
            file.save(destination)

            sequences = len([1 for line in open(destination) if line.startswith(">")])

            # get hash
            sha1hasher = FileHash('sha1')
            hashSha1 = sha1hasher.hash_file(destination)

            # insert database
            try:
                conn = db.engine.connect()
                conn.execute("INSERT INTO uploads(filename, sequences, sha1, date_created) VALUES (%s, %s, %s, %s)", (filename, sequences, hashSha1, datetime.now()))
            except SQLAlchemyError as e:
                print(str(e.__dict__['orig']))
                return {'error': 'Error! File already exists'}
            finally:
                conn.close()

            apiResult = {'filename': filename,'sequences': sequences,'sha1': hashSha1}
            return apiResult
        else:
            error = 'Invalid file type or not upload file.'
    return {'error': error}