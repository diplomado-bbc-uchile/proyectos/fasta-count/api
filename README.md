# API

- [API](#api)
  - [Requisitos](#requisitos)
  - [Ejecución](#ejecución)

## Requisitos

Run in **MySQL** database
```
CREATE DATABASE fastacount;
USE fastacount;
CREATE TABLE uploads (filename VARCHAR(50) NOT NULL, sequences INTEGER NOT NULL, sha1 VARCHAR(40) NOT NULL, date_created DATETIME NOT NULL, PRIMARY KEY (sha1));
```

La base de datos puede ser ejecutada de manera rápida con contenedores, comando:
```
docker run --name mysql \
    -p 3306:3306 \
    -v database:/var/lib/mysql \
    -e MYSQL_RANDOM_ROOT_PASSWORD=yes \
    -e MYSQL_DATABASE=fastacount \
    -e MYSQL_USER=fasta \
    -e MYSQL_PASSWORD=count \
    -d mysql:5.7
```

Instalar las dependencias del sistema
```
apk update; apk add py3-mysqlclient mysql-client;
```

Clonar el repositorio y entrar en la carpeta
```
git clone https://gitlab.com/diplomado-bbc-uchile/proyectos/fasta-count/api.git;
cd api;
```

Definir las variables de entorno
```
export FLASK_APP=api.py
export FLASK_RUN_PORT=5000
export MYSQL_HOST=127.0.0.1
export MYSQL_USER=fasta
export MYSQL_PASSWORD=count
export MYSQL_DB=fastacount
```

> Para hacer la comunicación con una BBDD MySQL con SSL, definir la variable `MYSQL_SSL_CA_PATH=/app/DigiCertGlobalRootCA.crt.pem`, esta opción funciona para Azure Database for MySQL.

## Ejecución

Ejecutar
```
flask run --host=0.0.0.0
```

Si todo sale bien, se debería ver un `alive` al acceder a la url `http://direccion-ip:5000`.